# -*- coding: utf-8 -*-
import html2text
import scrapy
import utils


class MarvinSpider(scrapy.Spider):
    name = 'marvinspider'
    start_urls = ['http://www.learnify.se/Hem/blog/Show/9359906']

    def parse(self, response):

        converter = html2text.HTML2Text()
        body = converter.handle(response.css('#blogContainer .blogPost .text').extract_first())

        utils.send_mail(subject='Marvins veckobrev', body=body)