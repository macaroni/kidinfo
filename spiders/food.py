# -*- coding: utf-8 -*-
import scrapy
import utils


class FoodSpider(scrapy.Spider):
    name = 'foodspider'
    start_urls = ['http://katarinasodraskola.stockholm.se']

    def parse(self, response):

        body = u''
        for food in response.css('.dishes').xpath('./li/text()').extract():
            body += food + '\n'

        if len(body) > 1:
            utils.send_mail(subject='Knattarnas lunch idag', body=body)