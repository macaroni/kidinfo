import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_mail(subject, body):
    msg = MIMEMultipart('mixed')
    msg['Subject'] = subject
    me = 'mail@omnipollo.com'
    to = ['joni@macaroni.se', 'janna.palmgren@creuna.se']
    msg['From'] = me
    msg['To'] = ", ".join(to)

    textPart = MIMEText(body, 'plain', 'utf-8')
    msg.attach(textPart)

    # Send the email via gmail SMTP server.
    try:
            smtpServer = smtplib.SMTP('smtp.gmail.com', 587)
            smtpServer.ehlo()
            smtpServer.starttls()
            smtpServer.ehlo()
            smtpServer.login('mail@omnipollo.com', 'mailmongo')
            smtpServer.sendmail(me, to, msg.as_string())
            smtpServer.close()
            print "mail sent."
    except Exception as e:
            print "failed to send mail %s" % e