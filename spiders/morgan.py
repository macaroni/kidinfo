# -*- coding: utf-8 -*-
import html2text
import scrapy
import utils


class MorganSpider(scrapy.Spider):
    name = 'morganspider'
    start_urls = ['http://www.learnify.se/Hem/blog/UnlockBlog/4607983']

    def parse(self, response):

        return scrapy.FormRequest.from_response(
            response,
            formdata={'LockCode': 'Växthuset6'},
            callback=self.after_login
        )

    def after_login(self, response):
        # check login succeed before going on
        if "authentication failed" in response.body:
            self.logger.error("Login failed")
            return

        print response.css('#blogContainer .blogPost').xpath('./h2').extract_first()
        converter = html2text.HTML2Text()
        body = converter.handle(response.css('#blogContainer .blogPost .text').extract_first())

        utils.send_mail(subject='Morgans veckobrev', body=body)